package main

import "gitlab.com/jhackenberg/broil/internal/app/backend/application"

func main() {
	app := application.NewApp()

	if err := app.Run(); err != nil {
		app.Logger.Fatal(err)
	}
}
