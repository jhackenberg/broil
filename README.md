# Broil

Broil is a process-based cookbook.

## Idea

Broil doesn't have recipes or ingredients. Instead, there are `processes`.
A process has different attributes and may be dependent on other processes.
A process without any dependencies is what you would consider an ingredient.

This allows for many interesting features such as
* calculating nutrition
* setting nutrition goals and generating diet plans
* [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)er recipes
* easy substitution of "ingredients"
* better time estimates
* generating complete recipes and customizing the level of detail
