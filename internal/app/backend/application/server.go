package application

import (
	"github.com/pkg/errors"
	"gitlab.com/jhackenberg/broil/internal/app/backend/server"
)

func (app *App) addServer() {
	s, err := server.NewServer(app.Config.Sub("server"), app.Logger)
	if err != nil {
		app.Logger.Fatal(errors.Wrap(err, "new server"))
	}

	app.Server = s
}
