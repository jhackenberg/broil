package application

import (
	"log"

	"gitlab.com/jhackenberg/broil/internal/app/backend/server"
	"gitlab.com/jhackenberg/broil/internal/pkg/application"
	"gitlab.com/jhackenberg/broil/internal/pkg/config/defaults"
)

// App is used to bundle all application resources.
type App struct {
	application.DefaultApp

	Server *server.Server
}

// NewApp returns a new App instance.
func NewApp() *App {
	app := &App{}

	if err := app.Init(application.Manifest{
		Name:           "broil-backend",
		DisplayName:    "Broil Backend",
		ConfigDefaults: []map[string]interface{}{defaults.Application},
	}); err != nil {
		log.Panicln(err)
	}

	app.addServer()

	return app
}
