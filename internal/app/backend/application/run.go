package application

import "github.com/pkg/errors"

func (app *App) Run() error {
	if err := app.Server.Run(); err != nil {
		return errors.Wrap(err, "server")
	}

	return nil
}
