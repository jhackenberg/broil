package server

import (
	"net/http"

	"gitlab.com/jhackenberg/broil/internal/app/backend/server/cors"
)

// setupRouter initializes the router during server initialization
func (server *Server) setupRouter() {
	server.addRoutes()
	server.addMiddlewares()
	server.useRouter()
}

// useRouter configures the application to use the server router
func (server *Server) useRouter() {
	c := cors.New(server.config.Sub("cors"))
	http.Handle("/", c.Handler(server.router))
}
