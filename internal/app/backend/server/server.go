package server

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/kataras/golog"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// Server bundles all server resources.
type Server struct {
	config *viper.Viper
	logger *golog.Logger
	router *mux.Router
}

// NewServer returns a new ready to run server instance.
func NewServer(conf *viper.Viper, logger *golog.Logger) (*Server, error) {
	server := &Server{
		config: conf,
		logger: logger,
		router: mux.NewRouter(),
	}

	server.setupRouter()

	return server, nil
}

// Run makes the server listen on the port specified in the config.
// It invokes http.ListenAndServe, hence using http.
// Use a reverse proxy in production.
func (server *Server) Run() error {
	if err := http.ListenAndServe(server.config.GetString("uri"), nil); err != nil {
		return errors.Wrap(err, " http listen and serve")
	}

	return nil
}
