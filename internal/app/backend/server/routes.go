package server

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/jhackenberg/broil/pkg/graph/generated"
	"gitlab.com/jhackenberg/broil/pkg/graph/resolver"
)

// addRoutes adds the routes to server router during server initialization
func (server *Server) addRoutes() {
	server.router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	server.router.Handle("/query", handler.NewDefaultServer(generated.NewExecutableSchema(
		generated.Config{Resolvers: &resolver.Resolver{}},
	)))
}
