package server

import (
	"gitlab.com/jhackenberg/broil/internal/app/backend/server/cors"
)

// addMiddlewares initializes the middlewares during server initialization
// and configures the router to use them.
func (server *Server) addMiddlewares() {
	server.router.Use(cors.New(server.config.Sub("cors")).Handler)
}
