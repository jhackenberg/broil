package cors

import (
	"github.com/rs/cors"
	"github.com/spf13/viper"
)

// New returns a new cors handler instance
func New(conf *viper.Viper) *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins:   conf.GetStringSlice("allowedOrigins"),
		AllowCredentials: conf.GetBool("allowCredentials"),
		Debug:            conf.GetBool("debug"),
	})
}
